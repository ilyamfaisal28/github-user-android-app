package com.ilyam.githubuser.api

import com.ilyam.githubuser.BuildConfig
import com.ilyam.githubuser.data.model.DetailUserResponse
import com.ilyam.githubuser.data.model.User
import com.ilyam.githubuser.data.model.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

const val API_KEY = BuildConfig.KEY

interface Api {

    @GET("search/users")
    @Headers("Authorization: token $API_KEY")
    fun getSearchUsers(
        @Query("q") query: String
    ): Call<UserResponse>

    @GET("users/{username}")
    @Headers("Authorization: token $API_KEY")
    fun getUsersDetail(
        @Path("username") username: String
    ): Call<DetailUserResponse>

    @GET("users/{username}/followers")
    @Headers("Authorization: token $API_KEY")
    fun getFollowers(
        @Path("username") username: String
    ): Call<ArrayList<User>>

    @GET("users/{username}/following")
    @Headers("Authorization: token $API_KEY")
    fun getFollowing(
        @Path("username") username: String
    ): Call<ArrayList<User>>
}