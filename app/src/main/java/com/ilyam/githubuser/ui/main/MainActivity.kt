package com.ilyam.githubuser.ui.main

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilyam.githubuser.R
import com.ilyam.githubuser.data.model.User
import com.ilyam.githubuser.databinding.ActivityMainBinding
import com.ilyam.githubuser.ui.detail.DetailUserActivity
import com.ilyam.githubuser.ui.favorite.FavoriteActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = UserAdapter()
        adapter.notifyDataSetChanged()

        adapter.setOnItemClickCallback(object : UserAdapter.OnItemClickCallback {
            override fun onItemClicked(data: User) {
                Intent(this@MainActivity, DetailUserActivity::class.java).also {
                    it.putExtra(DetailUserActivity.EXTRA_USERNAME, data.login)
                    it.putExtra(DetailUserActivity.EXTRA_ID, data.id)
                    it.putExtra(DetailUserActivity.EXTRA_URL, data.avatar_url)
                    it.putExtra(DetailUserActivity.EXTRA_HTML, data.html_url)
                    startActivity(it)
                }
            }
        })
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MainViewModel::class.java
        )

        binding.apply {
            recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.option_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.maxWidth = Int.MAX_VALUE

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = resources.getString(R.string.search_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                showLoading(true)
                viewModel.setSearchUsers(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                showLoading(false)
                return false
            }
        })
        viewModel.getSearchUsers().observe(this, {
            if (it == null || it.isEmpty()) {
                showErrorMessage(R.drawable.ic_baseline_person_search_24, getString(R.string.Username_tidak_ditemukan))
                showLoading(false)
            } else {
                if (binding.errorId.errorLayout.visibility == View.VISIBLE) {
                    binding.errorId.errorLayout.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                }
                adapter.setList(it)
                showLoading(false)
            }
        })

        viewModel.message.observe(this, { it ->
            it.getContentIfNotHandled()?.let {
                Toast.makeText(this, getString(R.string.periksa_koneksi), Toast.LENGTH_LONG).show()
                showErrorMessage(R.drawable.ic_baseline_wifi_off_24, it)
                showLoading(false)
            }
        })
        return true
    }

    private fun showLoading(state: Boolean) {
        binding.progressBar.visibility = if(state) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.about -> {
                val i = Intent(this, AboutActivity::class.java)
                startActivity(i)
                true
            }
            R.id.favorite -> {
                val i = Intent(this, FavoriteActivity::class.java)
                startActivity(i)
                true
            }
            R.id.theme -> {
                val i = Intent(this, ThemeActivity::class.java)
                startActivity(i)
                true
            }
            else -> true
        }
    }

    private fun showErrorMessage(imageView: Int, title: String){
        if (binding.errorId.errorLayout.visibility == View.GONE) {
            binding.errorId.errorLayout.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
        }
        binding.errorId.errorImage.setImageResource(imageView)
        binding.errorId.errorTitle.text = title
    }

}