package com.ilyam.githubuser.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ilyam.githubuser.api.RetrofitClient
import com.ilyam.githubuser.data.model.User
import com.ilyam.githubuser.data.model.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel(){

    val listUsers = MutableLiveData<ArrayList<User>>()

    private val statusMessage = MutableLiveData<Event<String>>()
    val message : LiveData<Event<String>>
        get() = statusMessage

    fun setSearchUsers(query: String){
        RetrofitClient.apiInstance
            .getSearchUsers(query)
            .enqueue(object : Callback<UserResponse>{
                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    if (response.isSuccessful && response.body()?.items != null){
                        listUsers.postValue(response.body()?.items)
                    }else{
                        statusMessage.value = Event("Server error")
                    }
                }

                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    Log.e("failure main", t.toString())
                    statusMessage.value = Event("Internet tidak tersambung")
                }
            })
    }

    fun getSearchUsers() : LiveData<ArrayList<User>>{
        return listUsers
    }
}