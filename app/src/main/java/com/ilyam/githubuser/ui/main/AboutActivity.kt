package com.ilyam.githubuser.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ilyam.githubuser.R


class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        val actionbar = supportActionBar
        actionbar?.title = getString(R.string.about)
        actionbar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}