package com.ilyam.githubuser.ui.favorite

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ilyam.githubuser.data.local.FavoriteUser
import com.ilyam.githubuser.data.local.FavoriteUserDao
import com.ilyam.githubuser.data.local.UserDatabase
import com.ilyam.githubuser.ui.main.Event

class FavoriteViewModel(application: Application) : AndroidViewModel(application) {

    private var userDao : FavoriteUserDao?
    private var userDb : UserDatabase? = UserDatabase.getDatabase(application)

    init{
        userDao = userDb?.favoriteUserDao()
    }

    fun getFavoriteUser(): LiveData<List<FavoriteUser>>?{
        return userDao?.getFavoriteUser()
    }

}