package com.ilyam.githubuser.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.marginTop
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilyam.githubuser.R
import com.ilyam.githubuser.databinding.FragmentFollowBinding
import com.ilyam.githubuser.ui.main.UserAdapter

class FollowersFragment : Fragment(R.layout.fragment_follow) {

    private var _binding : FragmentFollowBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: FollowersViewModel
    private lateinit var adapter: UserAdapter
    private lateinit var username: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentFollowBinding.bind(view)

        val args = arguments
        username = args?.getString(DetailUserActivity.EXTRA_USERNAME).toString()

        adapter = UserAdapter()
        adapter.notifyDataSetChanged()

        binding.apply {
            recyclerView.setHasFixedSize(true)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter
        }

        showLoading(true)
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(FollowersViewModel::class.java)
        viewModel.setListFollowers(username)
        viewModel.getListFollowers().observe(viewLifecycleOwner, {
            if (it == null || it.isEmpty()) {
                showErrorMessage(R.drawable.ic_baseline_people_24, getString(R.string.tidak_punya_follower))
                binding.recyclerView.visibility = View.VISIBLE
                showLoading(false)
            }else{
                if (binding.errorId.errorLayout.visibility == View.VISIBLE) {
                    binding.errorId.errorLayout.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                }
                adapter.setList(it)
                showLoading(false)
            }
        })

        viewModel.message.observe(DetailUserActivity(), { it ->
            it.getContentIfNotHandled()?.let {
                Toast.makeText(DetailUserActivity(), getString(R.string.periksa_koneksi), Toast.LENGTH_LONG).show()
                showErrorMessage(R.drawable.ic_baseline_wifi_off_24, it)
                showLoading(false)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showLoading(state: Boolean) {
        binding.progressBar.visibility = if(state) View.VISIBLE else View.GONE
    }

    private fun showErrorMessage(imageView: Int, title: String){
        if (binding.errorId.errorLayout.visibility == View.GONE) {
            binding.errorId.errorLayout.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
        }
        binding.errorId.errorImage.setImageResource(imageView)
        binding.errorId.errorTitle.text = title
    }
}