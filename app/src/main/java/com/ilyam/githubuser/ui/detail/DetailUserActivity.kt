package com.ilyam.githubuser.ui.detail

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.tabs.TabLayoutMediator
import com.ilyam.githubuser.R
import com.ilyam.githubuser.databinding.ActivityDetailUserBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailUserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailUserBinding
    private lateinit var viewModel: DetailUserViewModel
    private lateinit var preferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val actionbar = supportActionBar
        actionbar?.title = getString(R.string.Detail_User)
        actionbar?.setDisplayHomeAsUpEnabled(true)

        val username = intent.getStringExtra(EXTRA_USERNAME)
        val id = intent.getIntExtra(EXTRA_ID, 0)
        val avatarUrl = intent.getStringExtra(EXTRA_URL)
        val htmlUrl = intent.getStringExtra(EXTRA_HTML)

        val bundle = Bundle()
        bundle.putString(EXTRA_USERNAME, username)

        viewModel = ViewModelProvider(this).get(DetailUserViewModel::class.java)

        username?.let { viewModel.setUserDetail(it) }
        viewModel.getUserDetail().observe(this, {
            if (it != null) {
                binding.apply {
                    tvName.text = it.name
                    tvUsername.text = it.login
                    tvCompany.text = it.company
                    tvLocation.text = it.location
                    tvUrl.text = it.html_url
                    tvRepository.text = "${it.public_repos}"
                    tvGist.text = "${it.public_gists}"
                    tvItemFollowers.text = "${it.followers}"
                    tvItemFollowing.text = "${it.following}"
                    Glide.with(this@DetailUserActivity)
                        .load(it.avatar_url)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .fitCenter()
                        .circleCrop()
                        .into(imgUser)
                }
            }
        })

        val favoriteBtn = binding.favoriteBtn
        var isChecked = false
        CoroutineScope(Dispatchers.IO).launch {
            val count = viewModel.checkUser(id)
            withContext(Dispatchers.Main) {
                if (count != null) {
                    if (count > 0){
                        binding.favoriteBtn.isFavorite = true
                        isChecked = true
                    }else{
                        binding.favoriteBtn.isFavorite = false
                        isChecked = false
                    }
                }
            }
        }

        preferences = getSharedPreferences(TOAST, Context.MODE_PRIVATE)
        val isFirsTime = preferences.getBoolean(TOAST, false)
        favoriteBtn.setOnFavoriteChangeListener { _, favorite ->
            isChecked = !isChecked
            if (isChecked && favorite) {
                if (username != null && avatarUrl != null && htmlUrl != null) {
                    viewModel.addToFavorite(username, id, avatarUrl, htmlUrl)
                }

                if (!isFirsTime) {
                    val toast =
                        Toast.makeText(
                            this,
                            getString(R.string.ditambahkan_ke_favorit),
                            Toast.LENGTH_SHORT
                        )
                    toast.setGravity(Gravity.BOTTOM or Gravity.END, 285, 70)
                    toast.show()
                    preferences.edit().putBoolean(TOAST, true).apply()
                }

            }else {
                viewModel.removeFromFavorite(id)

                val toast2 = Toast.makeText(this, getString(R.string.dihapus_dari_favorit), Toast.LENGTH_SHORT)
                toast2.setGravity(Gravity.BOTTOM or Gravity.END, 285, 70)
                toast2.show()
            }
        }

        val sectionPagerAdapter = SectionPagerAdapter(this, bundle)
        binding.apply {
            viewPager.adapter = sectionPagerAdapter
            TabLayoutMediator(binding.tabs, viewPager) { tab, position ->
                tab.text = resources.getString(TAB_TITLES[position])
            }.attach()
            supportActionBar?.elevation = 0f
        }

        viewModel.message.observe(this, { it ->
            it.getContentIfNotHandled()?.let {
                Toast.makeText(this, getString(R.string.periksa_koneksi), Toast.LENGTH_LONG).show()
                showErrorMessage(R.drawable.ic_baseline_wifi_off_24, it)
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showErrorMessage(imageView: Int, title: String) {
        if (binding.errorId.errorLayout.visibility == View.GONE) {
            binding.errorId.errorLayout.visibility = View.VISIBLE
            binding.appBar.visibility = View.GONE
            binding.viewPager.visibility = View.GONE
            binding.cardView.visibility = View.GONE
        }
        binding.errorId.errorImage.setImageResource(imageView)
        binding.errorId.errorTitle.text = title
    }

    companion object {
        const val EXTRA_USERNAME = "extra_username"
        const val EXTRA_ID = "extra_id"
        const val EXTRA_URL = "extra_url"
        const val EXTRA_HTML = "extra_html"
        private const val TOAST = "toast"

        @StringRes
        private val TAB_TITLES = intArrayOf(
            R.string.tab_1,
            R.string.tab_2
        )
    }

}