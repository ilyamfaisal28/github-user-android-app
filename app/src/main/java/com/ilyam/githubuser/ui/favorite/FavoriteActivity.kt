package com.ilyam.githubuser.ui.favorite

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilyam.githubuser.R
import com.ilyam.githubuser.data.local.FavoriteUser
import com.ilyam.githubuser.data.model.User
import com.ilyam.githubuser.databinding.ActivityFavoriteBinding
import com.ilyam.githubuser.ui.detail.DetailUserActivity
import com.ilyam.githubuser.ui.main.UserAdapter

class FavoriteActivity : AppCompatActivity() {

    private lateinit var binding : ActivityFavoriteBinding
    private lateinit var adapter : UserAdapter
    private lateinit var viewModel: FavoriteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavoriteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val actionbar = supportActionBar
        actionbar?.title = getString(R.string.favorite_user)
        actionbar?.setDisplayHomeAsUpEnabled(true)

        adapter = UserAdapter()
        adapter.notifyDataSetChanged()

        viewModel = ViewModelProvider(this).get(FavoriteViewModel::class.java)

        adapter.setOnItemClickCallback(object : UserAdapter.OnItemClickCallback {
            override fun onItemClicked(data: User) {
                Intent(this@FavoriteActivity, DetailUserActivity::class.java).also {
                    it.putExtra(DetailUserActivity.EXTRA_USERNAME, data.login)
                    it.putExtra(DetailUserActivity.EXTRA_ID, data.id)
                    it.putExtra(DetailUserActivity.EXTRA_URL, data.avatar_url)
                    it.putExtra(DetailUserActivity.EXTRA_HTML, data.html_url)
                    startActivity(it)
                }
            }
        })

        binding.apply {
            rvUser.setHasFixedSize(true)
            rvUser.layoutManager = LinearLayoutManager(this@FavoriteActivity)
            rvUser.adapter = adapter
        }

        viewModel.getFavoriteUser()?.observe(this, {
            if (it == null || it.isEmpty()){
                showErrorMessage(R.drawable.ic_baseline_person_add_disabled_24, getString(R.string.favorit_kosong))
            } else{
                if (binding.errorId.errorLayout.visibility == View.VISIBLE) {
                    binding.errorId.errorLayout.visibility = View.GONE
                    binding.rvUser.visibility = View.VISIBLE
                }
                adapter.setList(mapList(it))
            }
        })
    }

    private fun mapList(users: List<FavoriteUser>): ArrayList<User> {
        val listUsers = ArrayList<User>()
        for (user in users){
            val userMapped = User(
                user.login,
                user.id,
                user.avatar_url,
                user.html_url
            )
            listUsers.add(userMapped)
        }
        return listUsers
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showErrorMessage(imageView: Int, title: String){
        if (binding.errorId.errorLayout.visibility == View.GONE) {
            binding.errorId.errorLayout.visibility = View.VISIBLE
            binding.rvUser.visibility = View.GONE
        }
        binding.errorId.errorImage.setImageResource(imageView)
        binding.errorId.errorTitle.text = title
    }
}