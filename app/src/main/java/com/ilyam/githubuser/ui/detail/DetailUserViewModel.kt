package com.ilyam.githubuser.ui.detail

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ilyam.githubuser.api.RetrofitClient
import com.ilyam.githubuser.data.local.FavoriteUser
import com.ilyam.githubuser.data.local.FavoriteUserDao
import com.ilyam.githubuser.data.local.UserDatabase
import com.ilyam.githubuser.data.model.DetailUserResponse
import com.ilyam.githubuser.ui.main.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailUserViewModel(application: Application) : AndroidViewModel(application){
    val user = MutableLiveData<DetailUserResponse>()

    private var userDao : FavoriteUserDao?
    private var userDb : UserDatabase? = UserDatabase.getDatabase(application)

    init{
        userDao = userDb?.favoriteUserDao()
    }

    private val statusMessage = MutableLiveData<Event<String>>()
    val message : LiveData<Event<String>>
        get() = statusMessage

    fun setUserDetail(username: String){
        RetrofitClient.apiInstance
            .getUsersDetail(username)
            .enqueue(object : Callback<DetailUserResponse>{
                override fun onResponse(
                    call: Call<DetailUserResponse>,
                    response: Response<DetailUserResponse>
                ) {
                    if (response.isSuccessful){
                        user.postValue(response.body())
                    }else {
                        statusMessage.value = Event("Server error")
                    }
                }

                override fun onFailure(call: Call<DetailUserResponse>, t: Throwable) {
                    Log.e("failure detail", t.toString())
                    statusMessage.value = Event("Internet tidak tersambung")
                }
            })
    }

    fun getUserDetail(): LiveData<DetailUserResponse>{
        return user
    }

    fun addToFavorite(username: String, id: Int, avatarUrl: String, htmlUrl: String){
        CoroutineScope(Dispatchers.IO).launch {
            val user = FavoriteUser(username, id, avatarUrl, htmlUrl)
            userDao?.addToFavorite(user)
        }
    }

    suspend fun checkUser(id: Int) = userDao?.checkUser(id)

    fun removeFromFavorite(id: Int){
        CoroutineScope(Dispatchers.IO).launch {
            userDao?.removeFromFavorite(id)
        }
    }
}