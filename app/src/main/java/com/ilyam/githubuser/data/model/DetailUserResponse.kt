package com.ilyam.githubuser.data.model

data class DetailUserResponse(
    val login: String,
    val id: Int,
    val avatar_url: String,
    val name: String,
    val followers_url: String,
    val following_url: String,
    val following: Int,
    val followers: Int,
    val public_gists: Int,
    val company: String,
    val location: String,
    val html_url: String,
    val public_repos: Int
)
