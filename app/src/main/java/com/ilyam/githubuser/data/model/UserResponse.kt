package com.ilyam.githubuser.data.model

data class UserResponse (
    val items : ArrayList<User>
)